import os
from dotenv import load_dotenv

# Load environment variables from .env file
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

# Set up configurations
SESSION_PERMANENT = os.getenv('SESSION_PERMANENT')
SESSION_TYPE = os.getenv('SESSION_TYPE')
USERS_DICTIONARY = os.getenv('USERS_DICTIONARY')

print("SESSION_PERMANENT:", SESSION_PERMANENT)
print("SESSION_TYPE:", SESSION_TYPE)
print("USERS_DICTIONARY:", USERS_DICTIONARY)
